from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil

tooltip  = "N/A"

# Compatibility with 4.6.3
try:
    PVUtil.PVHasNoValueException
except:
    setattr(PVUtil, 'PVHasNoValueException', Exception)

try:
    pvStat   = PVUtil.getString(pvs[0]).upper()
    pvPrsStr = PVUtil.getString(pvs[2])
    pvUnit   = PVUtil.getString(pvs[3])

    try:
        from org.phoebus.ui.vtype import FormatOption, FormatOptionHandler
    except ImportError:
        try:
            from org.csstudio.display.builder.model.properties import FormatOption
            from org.csstudio.display.builder.model.util import FormatOptionHandler
        except Exception as e:
            ScriptUtil.getLogger().severe(str(e))

    try:
        vtype = PVUtil.getVType(pvs[1])

        pressure = FormatOptionHandler.format(vtype, FormatOption.EXPONENTIAL, 2, True)
    except Exception as e:
        ScriptUtil.getLogger().severe(str(e))
        pressure = None

    if pvStat == "ON":
        if pressure is None:
            tooltip = pvPrsStr + " " + pvUnit
        else:
            tooltip = pressure
    elif pvStat == "OVER-RANGE" or pvStat == "UNDER-RANGE":
        if pressure is None:
            tooltip = str(PVUtil.getDouble(pvs[1])) + " " + pvUnit
        else:
            tooltip = pressure
    else:
        tooltip = pvPrsStr
except PVUtil.PVHasNoValueException:
    pass
except Exception as e:
    ScriptUtil.getLogger().severe(str(e))

widget.setPropertyValue("tooltip", tooltip)
