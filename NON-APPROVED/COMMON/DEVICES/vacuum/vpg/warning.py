#Generated from VACUUM_VAC-VPG.def at 2020-06-11_16:18:29
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Service Mode (Leak Detection) Over Time",
                 19 : "High Vacuum Pumping Starting Not Possible - All Systems are Locked",
                 18 : "1st High Vacuum Pumping System Error - Angle Valve Position Error",
                 17 : "2nd High Vacuum Pumping System Error - Angle Valve Position Error",
                 16 : "3rd High Vacuum Pumping System Error - Angle Valve Position Error",
                 15 : "4th High Vacuum Pumping System Error - Angle Valve Position Error",
                 14 : "5th High Vacuum Pumping System Error - Angle Valve Position Error",
                 11 : "Atmospheric & Vacuum Starting are Enabled",
                 10 : "No Starting Enabled",
                 9 : "Atmospheric Starting is Enabled",
                 8 : "VPSU-0020 24VDC Power Supply Overload",
                 7 : "VPSU-0020 24VDC Power Supply Tripped",
                 6 : "VPSU-0010 24VDC Power Supply Overload",
                 5 : "VPSU-0010 24VDC Power Supply Tripped",
                 4 : "VPSU-0010 400VAC 3-Phases Power Supply Tripped",
                 3 : "Automatic Restart On-going",
                 2 : "All Pumping Systems Locked - Starting Not Possible",
                 1 : "No Mode Selected",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Warning Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown warning code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
