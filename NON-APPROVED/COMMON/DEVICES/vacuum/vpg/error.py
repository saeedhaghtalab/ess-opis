#Generated from VACUUM_VAC-VPG.def at 2020-06-11_16:29:09
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Hardware Interlock",
                 98 : "Software Interlock",
                 97 : "Turbo-Pumps Controller Error",
                 96 : "Power Supply Error",
                 95 : "High Vacuum Manifold Vented - Pressure Interlock",
                 94 : "Low Vacuum Manifold Vented - Pressure Interlock",
                 92 : "Primary Pump Error",
                 91 : "Primary Pump / Valve Error",
                 90 : "Turbo-Pumps Not Available",
                 89 : "Max Auto-Restart",
                 88 : "Back-Up Primary Pumping System Error",
                 15 : "Mode Selection Error - Vacuum Sector Vented",
                 14 : "Mode Selection Error - Vacuum Sector Under Vacuum",
                 13 : "Primary Pumping System: Rescue Primary System is Off",
                 12 : "Primary Pumping System: Rescue Primary Not Available",
                 11 : "Primary Pumping System: Primary Pump / Valve Error",
                 10 : "Primary Pumping System: Start Disable",
                 9 : "VPSU-00020 400VAC 3-Phases Power Supply Tripped",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Error Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown error code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
