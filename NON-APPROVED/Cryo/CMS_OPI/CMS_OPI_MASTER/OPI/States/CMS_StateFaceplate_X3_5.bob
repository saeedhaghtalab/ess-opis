<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>CMS X3_5 Helium Purge</name>
  <macros>
    <PLCName>CrS-CMS:Cryo-PLC-01</PLCName>
  </macros>
  <width>1780</width>
  <height>1010</height>
  <background_color>
    <color name="WHITE" red="255" green="255" blue="255">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>X3_5 - Helium Purge</text>
    <width>1780</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1</name>
    <macros>
      <StepName>Maintenance</StepName>
      <WIDDev>Virt</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>X2</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>1410</x>
    <y>120</y>
    <width>330</width>
    <height>180</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>810</x>
    <y>80</y>
    <width>516</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>1410</x>
    <y>80</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>80</y>
    <width>700</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>CV-62005 open position prior to first step</Description>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>120</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>CMS depressurized pressure</Description>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>150</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>Waiting Time after depressurizing</Description>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>180</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1</name>
    <macros>
      <Description>Spare Flag 1</Description>
      <FlagID>Flag1</FlagID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>121</y>
    <width>530</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>First step pressurizing limit</Description>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>210</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter5</name>
    <macros>
      <Description>First step pressure test time</Description>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>240</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter6</name>
    <macros>
      <Description>Pressure test deadband</Description>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>270</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter7</name>
    <macros>
      <Description>Second step pressurizing limit</Description>
      <ParameterID>S7</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>300</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter8</name>
    <macros>
      <Description>Third step pressurizing limit</Description>
      <ParameterID>S8</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>330</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter9</name>
    <macros>
      <Description>Final step pressurizing limit</Description>
      <ParameterID>S9</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>360</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter10</name>
    <macros>
      <Description>CV-62005 open position after depressurizing</Description>
      <ParameterID>S10</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>390</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter11</name>
    <macros>
      <Description>Multiple cleaning process first step</Description>
      <ParameterID>S11</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>420</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter12</name>
    <macros>
      <Description>Waiting time between cleaning process steps</Description>
      <ParameterID>S12</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>450</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter13</name>
    <macros>
      <Description>Multiple cleaning process second step</Description>
      <ParameterID>S13</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>480</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter14</name>
    <macros>
      <Description>Waiting time between cleaning and depressurize</Description>
      <ParameterID>S14</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>510</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter15</name>
    <macros>
      <Description>Cleaning cycle repeat time</Description>
      <ParameterID>S15</ParameterID>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/StateMachine/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>540</y>
    <width>700</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ControlledDevicesLabel</name>
    <text>Controlled / Measured Devices</text>
    <x>30</x>
    <y>700</y>
    <width>1296</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PV-42620</name>
    <macros>
      <WIDDev>PV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>42620</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/Valves/PV_VALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>30</x>
    <y>750</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>CV_62005</name>
    <macros>
      <WIDDev>CV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62005</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/Valves/CV_ControlVALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>150</x>
    <y>750</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>DP-42150</name>
    <macros>
      <WIDDev>DP</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>42150</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/Pumps/DP_OnOffPump_BlockIcon_Vertical_Compact.bob</file>
    <x>450</x>
    <y>750</y>
    <width>105</width>
    <height>110</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-62075</name>
    <macros>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62075</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/AnalogMeasure/AnalogTransmitter_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>640</x>
    <y>750</y>
    <width>114</width>
    <height>48</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>CV_62006</name>
    <macros>
      <WIDDev>CV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>42330</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../ICS_OPI_LIBRARY/DeviceTypes/Valves/CV_ControlVALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>262</x>
    <y>750</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
</display>
